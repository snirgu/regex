import getopt
import sys
from File import File


def main(argv):
    files = []

    # check if -h exist
    if '-h' in argv or '--help' in argv:
        s = """        Description: Search for regex in file/s and output in different formats.
        Usage: regex.py -r|--regex <regex> -f|--file <file> [OPTION...] 
        Example: 'regex.py -r t+e -f filename.txt'
        MANDATORY:
            -f, --file  [-f, --file...]     file to search <if -h or --help not used, the user prompt opens> 
            -r, --regex                     regular expression pattern
        
        
        OPTIONS:
            -h, --help                      this help screen
            
            formats: <if none provided default format is used>
            -c, --color                     highlights matches in yellow
            -m, --machine                   machine readable output
            -u, --underscore                provide pointer '^' under matches
        """
        print(s)
        sys.exit(0)
    # check if -r exist
    if not '-r' in argv and not '--regex' in argv:
        print('No regex provided.. Try \'regex.py -h|--help\' for more information.')
        sys.exit(0)
    # check if -f or --file exist, otherwise call stdin
    if not '-f' in argv and not '--file' in argv:
        try:
            files.append(File(input('Please enter file/s to search: ')))
        except:
            print('if you are using python 2, use: \'<filename>\'')
            sys.exit(1)
    # parse arguments
    try:
        opts, args = getopt.getopt(argv, "r:f:umch", ["regex=", "file=", "underscore", "machine", "color", "help"])
        for opt, arg in opts:
            if opt in ("-r", "--regex"):
                regex = arg
            if opt in ("-f", "--file"):
                files.append(File(arg))
    except getopt.GetoptError:
        print('regex.py -r <regex> -f <files>')
        sys.exit(1)

    # run search by the type of format option selected
    for file in files:
        try:
            # "UNDERSCORE" format
            if '-u' in argv or '--underscore' in argv:
                file.search(regex, 'underscore')
            # "MACHINE" format
            elif '-m' in argv or '--machine' in argv:
                file.search(regex, 'machine')
            # "COLOR" format
            elif '-c' in argv or '--color' in argv:
                file.search(regex, 'color')
            else:
                file.search(regex, 'default')
        except FileNotFoundError:
            print('file not found.. exiting.')
            sys.exit(1)


if __name__ == "__main__":
    main(sys.argv[1:])
