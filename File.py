import re


class File:

    def __init__(self, filename):
        self.fileName = filename

    def search(self, pattern, format_output):

        for lineNumber, line in enumerate(open(self.fileName)):

            # "DEFAULT" format case
            if format_output == 'default':
                if re.search(pattern, line):
                    print('Match found on %s, on line %d: %s' % (self.fileName, lineNumber, line.rstrip()))

            # "UNDERSCORE" format case
            elif format_output == 'underscore':
                # ^^^creating pointer line^^^
                x = []
                for i in range(len(line)):
                    x.append(' ')
                for _ in re.findall(pattern, line):
                    for match in re.finditer(pattern, line):
                        for i in range(match.span()[0], match.span()[1]):
                            x[i] = "^"
                # ^^^end creating pointer line^^^

                if re.search(pattern, line):
                    print(line.rstrip())
                    print(''.join(x))

            # "MACHINE" format case
            elif format_output == 'machine':
                if re.search(pattern, line):
                    print(
                        '%s:%d:%s:%s' % (self.fileName, lineNumber, re.search(pattern, line).span()[0], line.rstrip()))

            # "COLOR" format case
            elif format_output == 'color':
                if re.search(pattern, line):
                    print('\033[0;39;39m%s line %s:%s\033[0;39;33m%s\033[0;39;39m%s' % (
                        self.fileName,  # filename
                        lineNumber,  # line number
                        line[:re.search(pattern, line).span()[0]],  # left to regex match
                        line[re.search(pattern, line).span()[0]:re.search(pattern, line).span()[1]],  # middle (regex match)
                        line[re.search(pattern, line).span()[1]:].rstrip()))  # right to regex match