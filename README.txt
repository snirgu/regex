# regex

regex is a Python script for searching regular expressions in file/s

## Usage

```
python regex.py -r <regex> -f <filename> [-f <filename> ...] [OPTIONS...]

Example: 'regex.py -r t+e -f filename.txt'

MANDATORY:
    -f, --file  [-f, --file...]     file to search
    -r, --regex                     regular expression pattern

OPTIONS:
    -h, --help                      this help screen

    formats: <if none provided default format is used>
    -c, --color                     highlights matches in yellow
    -m, --machine                   machine readable output
    -u, --underscore                provide pointer '^' under matches
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
